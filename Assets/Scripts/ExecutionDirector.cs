﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Scorewarrior.Test.Commands;

namespace Scorewarrior.Test
{
	public class ExecutionDirector : IExecutionDirector
	{
		private readonly Dictionary<Type, IExecutor<ICommand>> _executorByExecutableType;

		private class ExecutorWrapper<TCommand> : IExecutor<ICommand> where TCommand : class, ICommand
		{
			private readonly IExecutor<TCommand> _executor;

			public ExecutorWrapper(IExecutor<TCommand> executor)
			{
				_executor = executor;
			}

			public void Execute(ICommand executable)
			{
				if (executable is not TCommand command)
				{
					throw new ArgumentException($"Cannot execute command of type {executable.GetType()}");
				}
				_executor.Execute(command);
			}
		}

		public ExecutionDirector()
		{
			_executorByExecutableType = new Dictionary<Type, IExecutor<ICommand>>();
		}

		public void RegisterExecutor<TCommand, TExecutor>(TExecutor executor)
				where TCommand : class, ICommand
				where TExecutor : class, IExecutor<TCommand>
		{
			Type executableType = typeof(TCommand);
			if (_executorByExecutableType.ContainsKey(executableType))
			{
				throw new ArgumentException("Executor already registered");
			}
			_executorByExecutableType.Add(executableType, new ExecutorWrapper<TCommand>(executor));
		}

		public void Execute(ICommand command)
		{
			Type executableType = command.GetType();
			if (_executorByExecutableType.TryGetValue(executableType, out var executor))
			{
				executor.Execute(command);
			}
			else
			{
				throw new Exception("Executor not found");
			}
		}
	}
}